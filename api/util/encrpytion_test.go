package util

import "testing"

func TestEncryption(t *testing.T) {

	items := []string{
		"james", "alicia", "ruben",
	}

	for _, password := range items {

		encodedPass, err := GenerateEncoded(password)
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		correctPassword, err := ComparePasswordAndHash(password, encodedPass)
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		if !correctPassword {
			t.Errorf("Error: Values do not match for %v value", password)
		} else {
			t.Logf("Passed: Values match for %v value", password)
		}
	}

}
