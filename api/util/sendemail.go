package util

import (
	"log"
	"net/smtp"
)

const username = "youremail"
const password = "youremailpassword"
const host = "smtp.test.com"
const frontend = "frontendurl"

// SendEmail : send the reset link to user.
func SendEmail(userEmail, token string) error {

	auth := smtp.PlainAuth("", username, password, host)

	to := []string{userEmail}
	msg := []byte("To: " + userEmail + "\r\n" +
		"Subject: Reset your password\r\n" +
		"\r\n" +
		"Please for reset your password follow the link below: " + frontend + "/reset-password?token=" + token)

	err := smtp.SendMail("smtp.test.com:587", auth, username, to, msg)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil

}
