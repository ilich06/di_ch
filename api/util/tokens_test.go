package util

import "testing"

func TestToken(t *testing.T) {

	users := []string{
		"james", "alicia", "ruben",
	}

	for _, user := range users {
		token, err := GenerateJWT(user)
		if err != nil {
			t.Errorf("Error: %v", err)
		}
		userFromToken, err := GetUsernameOfToken(token)
		if err != nil {
			t.Errorf("Error: %v", err)
		}
		if user != userFromToken {
			t.Errorf("Failed, expected %v value but got %v value", user, userFromToken)
		} else {
			t.Logf("Passed, expected %v value and got %v value", user, userFromToken)
		}
	}

}
