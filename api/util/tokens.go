package util

import (
	"encoding/base64"
	"log"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	jsoniter "github.com/json-iterator/go"
)

// mySigningKey : Key to sign the JWT.
var mySigningKey = []byte("my-super-secret")

// GenerateJWT : Return a JWT from a username for manage session.
func GenerateJWT(username string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	// After 200 minutes the token is no longer valid.
	claims["exp"] = time.Now().Add(time.Minute * 200).Unix()

	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// GenerateResetJWT : Return a JWT from a username for manage password reset.
func GenerateResetJWT(username string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	// After 15 minutes the token is no longer valid.
	claims["exp"] = time.Now().Add(time.Minute * 15).Unix()

	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// ValidJWT : Verify that a JWT has not expired.
func ValidJWT(tokenInput string) bool {

	token, err := jwt.Parse(tokenInput, func(tokenInput *jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	})

	if err != nil {
		return false
	}

	if token.Valid {
		return true
	}

	return false

}

// PlayloadJWT : Used to extract token parameters.
type PlayloadJWT struct {
	Exp      int    `json:"exp"`
	Username string `json:"username"`
}

// GetUsernameOfToken : Returns the username of a JWT.
func GetUsernameOfToken(accessToken string) (string, error) {

	slice := strings.Split(accessToken, ".")
	decode, err := base64.RawURLEncoding.DecodeString(slice[1])
	if err != nil {
		log.Println(err)
		return "", err
	}

	var playloadJWT PlayloadJWT
	if err := jsoniter.Unmarshal(decode, &playloadJWT); err != nil {
		return "", err
	}

	return playloadJWT.Username, nil
}
