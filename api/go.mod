module api

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/futurenda/google-auth-id-token-verifier v0.0.0-20170311140316-2a5b89f28b7e
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/json-iterator/go v1.1.10
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
)
