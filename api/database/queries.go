package database

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// CustomUser : Used to handle specific users.
type CustomUser struct {
	Username string
	Password string
}

// Login : Used to handle specific login data.
type Login struct {
	Username string
	Password string
}

// User : Used to handle users.
type User struct {
	FullName  string `json:"username"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

// InsertCustomUser : Insert a non-google user to the database.
func InsertCustomUser(username, password string) error {

	stmt, err := db.Prepare("INSERT INTO users(email, user_password) VALUES(?, ?)")
	if err != nil {
		log.Println(err)
		return err
	}

	res, err := stmt.Exec(username, password)
	if err != nil {
		log.Println(err)
		return err
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
		return err
	}

	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil
}

// InsertGoogleUser : Insert a google user to the database.
func InsertGoogleUser(newUser User) error {

	stmt, err := db.Prepare("INSERT INTO users(full_name, email, google_account) VALUES(?, ?, ?)")
	if err != nil {
		log.Println(err)
		return err
	}

	// Insert the value '1' into the 'google_account' field because a google user is being inserted.
	res, err := stmt.Exec(newUser.FullName, newUser.Email, 1)
	if err != nil {
		log.Println(err)
		return err
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
		return err
	}

	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil
}

// ExistsUser : Verify if a certain user exists.
func ExistsUser(username string) (bool, error) {
	var result int

	stmt, err := db.Prepare("SELECT COUNT(1) FROM users WHERE email = ?")
	if err != nil {
		log.Println(err)
		return false, err
	}

	defer stmt.Close()
	if err := stmt.QueryRow(username).Scan(&result); err != nil {
		log.Println(err)
		return false, err
	}

	if result == 1 {
		log.Println(err)
		return true, nil
	}

	return false, nil

}

// IsGoogleUser : Check if the user is a google user.
func IsGoogleUser(username string) (bool, error) {

	var result int

	stmt, err := db.Prepare("SELECT google_account FROM users WHERE email = ?")
	if err != nil {
		log.Println(err)
		return false, err
	}

	defer stmt.Close()
	if err := stmt.QueryRow(username).Scan(&result); err != nil {
		log.Println(err)
		return false, err
	}

	if result == 1 {
		log.Println(err)
		return true, nil
	}

	return false, nil

}

// Profile : Used for handle user profile.
type Profile struct {
	FullName      string
	Address       string
	Telephone     string
	Username      string
	GoogleAccount bool
}

// GetProfile : Return a user's profile.
func GetProfile(username string) (Profile, error) {

	var (
		profile       Profile
		fullName      string
		email         string
		address       string
		telephone     string
		googleAccount int
	)

	stmt, err := db.Prepare("SELECT full_name, address, telephone, email, google_account FROM users WHERE email = ?")
	if err != nil {
		log.Println(err)
		return profile, err
	}

	defer stmt.Close()

	if err := stmt.QueryRow(username).Scan(&fullName, &address, &telephone, &email, &googleAccount); err != nil {
		log.Println(err)
		return profile, err
	}

	if googleAccount == 1 {
		profile = Profile{fullName, address, telephone, email, true}
	} else {
		profile = Profile{fullName, address, telephone, email, false}
	}

	return profile, nil

}

// InsertProfile : Insert a user's profile to the database.
func InsertProfile(newProfile Profile, username string) error {
	stmt, err := db.Prepare("UPDATE users SET full_name = ?, address = ?, telephone = ?, email = ? WHERE email = ?")
	if err != nil {
		log.Println(err)
		return err
	}
	res, err := stmt.Exec(newProfile.FullName, newProfile.Address, newProfile.Telephone, newProfile.Username, username)
	if err != nil {
		log.Println(err)
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return err
	}
	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)

	if newProfile.Username != username {
		if err := updateUserToken(username, newProfile.Username); err != nil {
			log.Println(err)
		}
		return err
	}

	return nil
}

// GetToken : Return the JWT of a user.
func GetToken(username string) (string, error) {
	var token string
	stmt, err := db.Prepare("SELECT token FROM tokens WHERE email = ?")
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer stmt.Close()
	if err := stmt.QueryRow(username).Scan(&token); err != nil {
		log.Println(err)
		return "", err
	}

	return token, nil
}

// GetPassword : Return a user's 'encrypted' password.
func GetPassword(username string) (string, error) {
	var password string
	stmt, err := db.Prepare("SELECT user_password FROM users WHERE email = ?")
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer stmt.Close()
	if err := stmt.QueryRow(username).Scan(&password); err != nil {
		log.Println(err)
		return "", err
	}

	return password, nil
}

// UpdatePassword : Update a user's password.
func UpdatePassword(username, password string) error {
	stmt, err := db.Prepare("UPDATE users SET user_password = ? WHERE email = ?")
	if err != nil {
		log.Println(err)
		return err
	}
	res, err := stmt.Exec(password, username)
	if err != nil {
		log.Println(err)
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return err
	}
	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil
}

// InsertToken : Insert a user's JWT into the database.
func InsertToken(username, token string) error {

	stmt, err := db.Prepare("INSERT INTO tokens VALUES(?, ?)")
	if err != nil {
		return err
	}
	res, err := stmt.Exec(username, token)
	if err != nil {
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil

}

// UpdateToken : Update the user's JWT into the database.
func UpdateToken(username, token string) error {

	stmt, err := db.Prepare("UPDATE tokens SET token = ? WHERE email = ?")
	if err != nil {
		return err
	}
	res, err := stmt.Exec(token, username)
	if err != nil {
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		return err
	}

	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil

}

// updateUserToken : Update username into tokens table.
func updateUserToken(username, newUsername string) error {

	stmt, err := db.Prepare("UPDATE tokens SET email = ? WHERE email = ?")
	if err != nil {
		return err
	}
	res, err := stmt.Exec(newUsername, username)
	if err != nil {
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		return err
	}

	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil

}

// DeleteToken : Delete a user's JWT into the database.
func DeleteToken(username string) error {
	stmt, err := db.Prepare("DELETE FROM tokens WHERE email = ?")
	if err != nil {
		return err
	}
	res, err := stmt.Exec(username)
	if err != nil {
		return err
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	return nil
}
