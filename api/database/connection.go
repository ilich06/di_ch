package database

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

const dbUsername = "yourusername"
const dbPassword = "yourpassword"
const dbname = "yourdb"

// OpenConnection : Open a connection to communicate with the database.
func OpenConnection() error {

	const url = dbUsername + ":" + dbPassword + "@tcp(localhost)/" + dbname

	var err error
	db, err = sql.Open("mysql", url)

	if err != nil {
		log.Println("Failed to connnect database.")
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}

	log.Println("Sucessful connection.")
	return nil

}
