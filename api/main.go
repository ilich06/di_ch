package main

import (
	"api/database"
	"api/handler"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func init() {
	if err := database.OpenConnection(); err != nil {
		log.Println(err)
	}
}

func main() {
	log.Println("Hello from Backend!")
	router := mux.NewRouter()
	// Authentication
	router.HandleFunc("/api/v1/login", handler.Login).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/google-log-in", handler.GoogleLogin).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/logout", handler.Logout).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/send-reset-link", handler.SendResetLink).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/reset-password", handler.ResetPassword).Methods(http.MethodPost)
	// Register
	router.HandleFunc("/api/v1/custom-sign-up", handler.CustomSignUp).Methods(http.MethodPost)
	router.HandleFunc("/api/v1/google-sign-up", handler.GoogleSignUp).Methods(http.MethodPost)
	// UserProfile
	router.HandleFunc("/api/v1/profile", handler.GetProfile).Methods(http.MethodGet)
	router.HandleFunc("/api/v1/profile", handler.PostProfile).Methods(http.MethodPost)
	log.Fatal(http.ListenAndServe(":8000", router))
}
