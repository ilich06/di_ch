package authentication

import (
	"log"

	googleAuthIDTokenVerifier "github.com/futurenda/google-auth-id-token-verifier"
)

// googleClientID : It's the id to access Oauth2
const googleClientID = "yourgoogleapikey"

// GetGoogleData : First, verify the token of the user logged in with Google, and then return their full name and email.
func GetGoogleData(token string) (string, string, error) {

	v := googleAuthIDTokenVerifier.Verifier{}
	aud := googleClientID

	if err := v.VerifyIDToken(token, []string{aud}); err != nil {
		log.Println(err)
		return "", "", err
	}

	claimSet, err := googleAuthIDTokenVerifier.Decode(token)
	if err != nil {
		log.Println(err)
		return "", "", err
	}

	return claimSet.Name, claimSet.Email, nil

}
