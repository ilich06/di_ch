package handler

import (
	"api/database"
	authentication "api/googleapi"
	"api/util"
	"log"
	"net/http"
	"time"

	jsoniter "github.com/json-iterator/go"
)

// Login : Handle the API login process.
func Login(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/login")

	var loginData database.Login

	if err := jsoniter.NewDecoder(r.Body).Decode(&loginData); err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if the user exists.
	existsUser, err := database.ExistsUser(loginData.Username)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if !existsUser {
		http.Error(w, "User not registered.", http.StatusUnauthorized)
		return
	}

	// Check if 'user' a google user.
	isGoogleUser, err := database.IsGoogleUser(loginData.Username)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if isGoogleUser {
		http.Error(w, "Please use 'Google Log-In' button", http.StatusBadRequest)
		return
	}

	// Get the password from the database.
	passwordHashed, err := database.GetPassword(loginData.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check password.
	matchPasswords, err := util.ComparePasswordAndHash(loginData.Password, passwordHashed)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if !matchPasswords {
		http.Error(w, "Password doesn't match!", http.StatusUnauthorized)
		return
	}

	// Generate JWT for manage sesion.
	token, err := util.GenerateJWT(loginData.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if err := database.InsertToken(loginData.Username, token); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Set cookie to response.
	cookie := http.Cookie{Name: "access_token", Value: token}
	http.SetCookie(w, &cookie)

}

// GoogleLogin : Handle the API login process for google users.
func GoogleLogin(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/googleLogin")

	var googleToken GoogleToken

	if err := jsoniter.NewDecoder(r.Body).Decode(&googleToken); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Get email of user.
	_, email, err := authentication.GetGoogleData(googleToken.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if the user exists.
	existsUser, err := database.ExistsUser(email)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if !existsUser {
		http.Error(w, "User not registered", http.StatusUnauthorized)
		return
	}

	// Generate JWT for manage sesion.
	token, err := util.GenerateJWT(email)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if err := database.InsertToken(email, token); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Set cookie to response.
	cookie := http.Cookie{Name: "access_token", Value: token}
	http.SetCookie(w, &cookie)

}

// Logout : Handle the API logout process.
func Logout(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/logout")

	// Get cookie from request.
	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	token := cookie.Value
	username, err := util.GetUsernameOfToken(token)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Delete the JWT from the database to remove the session.
	if err := database.DeleteToken(username); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	newCookie := http.Cookie{Name: "access_token", Value: "", Path: "/", Expires: time.Unix(0, 0)}
	http.SetCookie(w, &newCookie)

}

// ResetPwd : Used for password reset.
type ResetPwd struct {
	Username    string
	NewPassword string
	Token       string
}

// SendResetLink : Handle the process of sending the reset email to user.
func SendResetLink(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/send-reset-link")

	var user ResetPwd

	if err := jsoniter.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if the user exists.
	existsUser, err := database.ExistsUser(user.Username)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if !existsUser {
		http.Error(w, "The entered user does not exist", http.StatusBadRequest)
		return
	}

	isGoogleUser, err := database.IsGoogleUser(user.Username)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if isGoogleUser {
		http.Error(w, "You cannot change your password, you are registered with a google account.", http.StatusBadRequest)
		return
	}

	// Generate a JWT for manage reset password process.
	token, err := util.GenerateResetJWT(user.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if err := util.SendEmail(user.Username, token); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

}

// ResetPassword : Handle the API reset password process.
func ResetPassword(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/reset-password")

	var user ResetPwd

	if err := jsoniter.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Verify that the JWT has not expired.
	validToken := util.ValidJWT(user.Token)
	if !validToken {
		http.Error(w, "Link expired, get another link", http.StatusBadRequest)
		return
	}

	username, err := util.GetUsernameOfToken(user.Token)
	if err != nil {
		log.Println(err)
	}

	// 'Encrypt' the password to store in the database.
	passwordHashed, err := util.GenerateEncoded(user.NewPassword)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Update the password in the database.
	if err := database.UpdatePassword(username, passwordHashed); err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

}
