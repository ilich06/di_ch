package handler

import (
	"api/database"
	"api/util"
	"log"
	"net/http"

	jsoniter "github.com/json-iterator/go"
)

// GetProfile : Handle for the process to get a user's profile.
func GetProfile(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/profile")

	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	token := cookie.Value
	// Extract the username from the token.
	username, err := util.GetUsernameOfToken(token)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if token is in database.
	tokenFromDB, err := database.GetToken(username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if token != tokenFromDB {
		http.Error(w, "Token not exists in database", http.StatusUnauthorized)
		return
	}

	// Get user's profile from database.
	profile, err := database.GetProfile(username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Set body response.
	respBody, err := jsoniter.Marshal(profile)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Write(respBody)

}

// PostProfile : Handle the process of updating a profile.
func PostProfile(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/profile")

	var newProfile database.Profile
	if err := jsoniter.NewDecoder(r.Body).Decode(&newProfile); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	token := cookie.Value
	// Extract the username from the token.
	username, err := util.GetUsernameOfToken(token)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if username != newProfile.Username {
		// Check if the new user exists.
		existsUser, err := database.ExistsUser(newProfile.Username)
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		if existsUser {
			http.Error(w, "This email is already in use", http.StatusBadRequest)
			return
		}
	}

	// Update user's profile in database.
	if err := database.InsertProfile(newProfile, username); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Generate new JWT.
	newToken, err := util.GenerateJWT(newProfile.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if err := database.UpdateToken(newProfile.Username, newToken); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Set cookie to response.
	newCookie := http.Cookie{Name: "access_token", Value: newToken}
	http.SetCookie(w, &newCookie)

}
