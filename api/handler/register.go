package handler

import (
	"api/database"
	authentication "api/googleapi"
	"api/util"
	"log"
	"net/http"

	jsoniter "github.com/json-iterator/go"
)

// CustomSignUp : Handle the sign-up process for a non-google user.
func CustomSignUp(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/custom-sign-up")

	var customUser database.CustomUser

	if err := jsoniter.NewDecoder(r.Body).Decode(&customUser); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if user exists in database.
	existsUser, err := database.ExistsUser(customUser.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if existsUser {
		http.Error(w, "This email is already in use, please log-in.", http.StatusBadRequest)
		return
	}

	// Generate 'encrypted' password.
	password, err := util.GenerateEncoded(customUser.Password)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Insert new user into database.
	if err := database.InsertCustomUser(customUser.Username, password); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Generate a JWT for manage session.
	token, err := util.GenerateJWT(customUser.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Insert JWT into database.
	if err := database.InsertToken(customUser.Username, token); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	cookie := http.Cookie{Name: "access_token", Value: token}
	http.SetCookie(w, &cookie)

}

// GoogleToken : Used for google token id.
type GoogleToken struct {
	ID string `json:"id"`
}

// GoogleSignUp : Handle the sign-up process for a google user.
func GoogleSignUp(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /api/v1/google-sign-up")

	var googleToken GoogleToken

	if err := jsoniter.NewDecoder(r.Body).Decode(&googleToken); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Get data from google token id.
	fullName, email, err := authentication.GetGoogleData(googleToken.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Check if user exists in database.
	existsUser, err := database.ExistsUser(email)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	if existsUser {
		http.Error(w, "This email is already in use, please log-in", http.StatusBadRequest)
		return
	}

	newUser := database.User{FullName: fullName, Email: email}

	// Insert new google user into database.
	if err := database.InsertGoogleUser(newUser); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Generate JWT for manage session.
	token, err := util.GenerateJWT(newUser.Email)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// Insert JWT into database.
	if err := database.InsertToken(newUser.Email, token); err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	cookie := http.Cookie{Name: "access_token", Value: token}
	http.SetCookie(w, &cookie)

}
