package main

import (
	"frontend/handler"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	log.Println("Hello from Frontend!")
	router := mux.NewRouter()
	fs := http.FileServer(http.Dir("assets/"))
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs))
	//Authentication
	router.HandleFunc("/login", handler.GetLogin).Methods("GET")
	router.HandleFunc("/login", handler.PostLogin).Methods("POST")
	router.HandleFunc("/google-log-in", handler.GoogleLogin).Methods("POST")
	router.HandleFunc("/logout", handler.Logout)
	router.HandleFunc("/forgot-password", handler.GetForgotPassword).Methods("GET")
	router.HandleFunc("/forgot-password", handler.PostForgotPassword).Methods("POST")
	router.HandleFunc("/reset-password", handler.GetResetPassword).Methods("GET")
	router.HandleFunc("/reset-password", handler.PostResetPassword).Methods("POST")
	//Register
	router.HandleFunc("/signup", handler.GetSignup).Methods("GET")
	router.HandleFunc("/signup", handler.PostSignup).Methods("POST")
	router.HandleFunc("/google-sign-up", handler.GoogleSignUp).Methods("POST")
	//userProfile
	router.HandleFunc("/profile", handler.GetProfile).Methods("GET")
	router.HandleFunc("/edit-profile", handler.GetEditProfile).Methods("GET")
	router.HandleFunc("/edit-profile", handler.PostEditProfile).Methods("POST")
	//Test
	log.Fatal(http.ListenAndServe(":80", router))
}
