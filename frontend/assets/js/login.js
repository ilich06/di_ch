init();

function init() {
  // Personal data
  const googleID = "google-client-id"
  gapi.load("auth2", function () {
    auth2 = gapi.auth2.init({
      client_id:
        googleID,
    });
    auth2.attachClickHandler(
      document.getElementById("customBtn"),
      {},
      onSuccess,
      onFailure
    );
  });
}

function onSuccess(googleUser) {
  console.log("On Success");
  document.getElementById("token_id").value = googleUser.getAuthResponse().id_token;
  document.getElementById("google_form").submit();
}

function onFailure(error) {
  console.log(error);
}