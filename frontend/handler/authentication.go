package handler

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	jsoniter "github.com/json-iterator/go"
)

// Login : Used for manage login data.
type Login struct {
	Username string
	Password string
	Err      bool
	ErrText  string
}

// GetLogin : Handle the rendering of login's template.
func GetLogin(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /login GET")

	var loginError Login
	tmpl := template.Must(template.ParseFiles("assets/html/login.html"))
	tmpl.Execute(w, loginError)

}

// PostLogin : Communicate with the backend server.
func PostLogin(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /login POST")

	username := r.FormValue("username")
	password := r.FormValue("password")

	loginData := Login{Username: username, Password: password}

	requestBody, err := jsoniter.Marshal(loginData)
	if err != nil {
		log.Println(err)
	}

	// Prepare the request for the backend server.
	resp, err := http.Post("http://localhost:8000/api/v1/login", "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	respCode := resp.StatusCode
	// Verify the response code from backend server.
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errorMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		loginError := Login{Username: username, Password: password, Err: true, ErrText: string(errorMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/login.html"))
		tmpl.Execute(w, loginError)
	} else {
		// Redirect to the main profile page.
		http.SetCookie(w, resp.Cookies()[0])
		http.Redirect(w, r, "http://localhost/profile", http.StatusSeeOther)
	}

}

// GoogleLogin : Handle the process of loging with google.
func GoogleLogin(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /google-login GET")

	googleToken := GoogleToken{r.FormValue("token_id")}
	reqBody, err := jsoniter.Marshal(googleToken)
	if err != nil {
		log.Println(err)
	}

	// Prepare the request for the backend server.
	resp, err := http.Post("http://localhost:8000/api/v1/google-log-in", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Println(err)
	}

	respCode := resp.StatusCode
	// Verify the response code from backend server.
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errorMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		loginError := Login{Err: true, ErrText: string(errorMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/login.html"))
		tmpl.Execute(w, loginError)
	} else {
		// Redirect to the main profile page.
		http.SetCookie(w, resp.Cookies()[0])
		http.Redirect(w, r, "http://localhost/profile", http.StatusSeeOther)
	}
}

// Logout : Handle the process of logout.
func Logout(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /logout POST")

	if r.Method == http.MethodGet {
		cookie, err := r.Cookie("access_token")
		if err != nil {
			log.Println(err)
		}
		// Prepare the request for the backend server.
		reqBody := strings.NewReader("")
		req, err := http.NewRequest("POST", "http://localhost:8000/api/v1/logout", reqBody)
		if err != nil {
			log.Println(err)
		}
		// Add cookie into request.
		req.AddCookie(cookie)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Println(err)
		}
		respCode := resp.StatusCode
		// Verify the response code from backend server.
		if respCode == http.StatusOK {
			// Redirect to the login profile page.
			http.SetCookie(w, resp.Cookies()[0])
			http.Redirect(w, r, "http://localhost/login", http.StatusSeeOther)
			return
		}
	}
}

// ResetPassword : Used for manage data for password reset.
type ResetPassword struct {
	Username    string
	NewPassword string
	Err         bool
	ErrText     string
}

// GetForgotPassword : Handle the rendering of forgot password's template.
func GetForgotPassword(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /forgot-password GET")

	tmpl := template.Must(template.ParseFiles("assets/html/forgot-password.html"))
	tmpl.Execute(w, nil)

}

// PostForgotPassword : Communicate with the backend server.
func PostForgotPassword(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /forgot-password POST")

	username := r.FormValue("username")

	user := ResetPassword{Username: username}

	reqBody, err := jsoniter.Marshal(user)
	if err != nil {
		log.Println(err)
	}

	// Prepare the request for the backend server.
	resp, err := http.Post("http://localhost:8000/api/v1/send-reset-link", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	// Verify the response code from backend server.
	respCode := resp.StatusCode
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errorMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		resetError := ResetPassword{Username: username, Err: true, ErrText: string(errorMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/forgot-password.html"))
		tmpl.Execute(w, resetError)
	} else {
		// Redirect to the link sent page.
		tmpl := template.Must(template.ParseFiles("assets/html/link-sent.html"))
		tmpl.Execute(w, user)
	}

}

// ResetPwdError : Used for manage data of password reset process.
type ResetPwdError struct {
	Password    string
	PasswordAux string
	Err         bool
	ErrText     string
	Token       string
}

// GetResetPassword : Handle the rendering of reset password's template.
func GetResetPassword(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /reset-password GET")

	token := r.URL.Query().Get("token")

	// Token is not present, redirect to get a password recovery link.
	if token == "" {
		http.Redirect(w, r, "http://localhost/forgot-password", http.StatusSeeOther)
		return
	}

	resetPassword := ResetPwdError{Token: token}

	tmpl := template.Must(template.ParseFiles("assets/html/reset-password.html"))
	tmpl.Execute(w, resetPassword)

}

// NewPassword : Used for manage data of new password process.
type NewPassword struct {
	NewPassword string
	Token       string
}

// PostResetPassword : Handle the process of password reset with the backend server.
func PostResetPassword(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /reset-password POST")

	password := r.FormValue("password")
	passwordAux := r.FormValue("passwordAux")
	token := r.FormValue("token")

	// Check if the passwords on the form match.
	if password != passwordAux {
		// Render the same page again with the corresponding errors.
		resetError := ResetPwdError{password, passwordAux, true, "Passwords do not match", token}
		tmpl := template.Must(template.ParseFiles("assets/html/reset-password.html"))
		tmpl.Execute(w, resetError)
		return
	}

	resetPassword := NewPassword{password, token}

	reqBody, err := jsoniter.Marshal(resetPassword)
	if err != nil {
		log.Println(err)
	}

	// Prepare the request for the backend server.
	resp, err := http.Post("http://localhost:8000/api/v1/reset-password", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	respCode := resp.StatusCode
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errorMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		resetError := ResetPwdError{Err: true, ErrText: string(errorMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/reset-password.html"))
		tmpl.Execute(w, resetError)
	} else {
		// Redirect to the login page.
		http.Redirect(w, r, "http://localhost/login", http.StatusSeeOther)
	}

}
