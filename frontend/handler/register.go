package handler

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	jsoniter "github.com/json-iterator/go"
)

// NewUser : Used for manage data of new user.
type NewUser struct {
	Username string
	Password string
}

// GetSignup : Handle the rendering of sign-up's template.
func GetSignup(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /signup GET")

	tmpl := template.Must(template.ParseFiles("assets/html/signup.html"))
	tmpl.Execute(w, nil)

}

// PostSignup : Handle the process of sign up in the backend server.
func PostSignup(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /signup POST")

	username := r.FormValue("username")
	password := r.FormValue("password")
	// Check values in form.
	if username != "" && password != "" {
		newUser := NewUser{username, password}
		requestBody, err := jsoniter.Marshal(newUser)
		if err != nil {
			log.Println(err)
		}
		// Prepare the request for the backend server.
		resp, err := http.Post("http://localhost:8000/api/v1/custom-sign-up", "application/json", bytes.NewBuffer(requestBody))
		if err != nil {
			log.Println(err)
		}
		respCode := resp.StatusCode
		// Verify the response code from backend server.
		if respCode != http.StatusOK {
			// Render the same page again with the corresponding errors.
			signUp := SignUp{Username: username, Err: true, ErrText: "This email is already in use."}
			tmpl := template.Must(template.ParseFiles("assets/html/signup.html"))
			tmpl.Execute(w, signUp)
		} else {
			// Redirect to the edit profile page.
			http.SetCookie(w, resp.Cookies()[0])
			http.Redirect(w, r, "http://localhost/edit-profile", http.StatusSeeOther)
		}
	} else {
		// Render the same page again with the corresponding errors.
		signUp := SignUp{Err: true, ErrText: "Please fill in all fields."}
		tmpl := template.Must(template.ParseFiles("assets/html/signup.html"))
		tmpl.Execute(w, signUp)
	}

}

// SignUp : used for manage sign-up data.
type SignUp struct {
	Username string
	Password string
	Err      bool
	ErrText  string
}

// GoogleToken : used for managa google token id.
type GoogleToken struct {
	ID string `json:"id"`
}

// GoogleSignUp : Handle the process of sign-up with google.
func GoogleSignUp(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /google-sign-up GET")

	log.Println("Hit googleSignUp")

	googleToken := GoogleToken{r.FormValue("token_id")}
	reqBody, err := jsoniter.Marshal(googleToken)
	if err != nil {
		log.Println(err)
	}
	// Prepare the request for the backend server.
	resp, err := http.Post("http://localhost:8000/api/v1/google-sign-up", "application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Println(err)
	}
	respCode := resp.StatusCode
	// Verify the response code from backend server.
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		signupError := SignUp{Err: true, ErrText: string(errMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/signup.html"))
		tmpl.Execute(w, signupError)
	} else {
		// Redirect to the edit profile page.
		http.SetCookie(w, resp.Cookies()[0])
		http.Redirect(w, r, "http://localhost/edit-profile", http.StatusSeeOther)
	}
}
