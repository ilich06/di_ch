package handler

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	jsoniter "github.com/json-iterator/go"
)

// Profile : Used for manage user's profile data.
type Profile struct {
	FullName      string
	Address       string
	Telephone     string
	Username      string
	GoogleAccount bool
	Err           bool
	ErrText       string
}

// GetProfile : Handle the process of get profile from backend server.
func GetProfile(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /profile GET")

	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	reqBody := strings.NewReader("")
	// Prepare the request for the backend server.
	req, err := http.NewRequest("GET", "http://localhost:8000/api/v1/profile", reqBody)
	if err != nil {
		log.Println(err)
	}
	// Add cookie into request.
	req.AddCookie(cookie)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
	}
	// Verify the response code from backend server.
	respCode := resp.StatusCode
	if respCode != http.StatusOK {
		// Redirect to login page.
		http.Redirect(w, r, "http://localhost/login", http.StatusSeeOther)
	} else {
		var profile Profile
		if err := jsoniter.NewDecoder(resp.Body).Decode(&profile); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// Insert headers so that the page is not cached and force the page to be reprocessed from scratch.
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Header().Set("Cache-Control", "private, no-cache, no-store, must-revalidate")
		w.Header().Set("Expires", "Sat, 01 Jan 2000 00:00:00 GMT")
		w.Header().Set("Pragma", "no-cache")
		// Render user profile page.
		tmpl := template.Must(template.ParseFiles("assets/html/profile.html"))
		tmpl.Execute(w, profile)
	}
}

// GetEditProfile : Handle the process of edit profile page.
func GetEditProfile(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /edit-profile GET")

	cookie, err := r.Cookie("access_token")
	if err != nil {
		log.Println(err)
		// If the token is not present, redirect to the login page.
		http.Redirect(w, r, "http://localhost/login", http.StatusSeeOther)
		return
	}
	reqBody := strings.NewReader("")
	// Prepare the request for the backend server.
	req, err := http.NewRequest("GET", "http://localhost:8000/api/v1/profile", reqBody)
	if err != nil {
		log.Println(err)
	}
	// Add cookie into request.
	req.AddCookie(cookie)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
	}
	// Verify the response code from backend server.
	respCode := resp.StatusCode
	if respCode != http.StatusOK {
		http.Redirect(w, r, "http://localhost/login", http.StatusSeeOther)
	} else {
		var profile Profile
		if err := jsoniter.NewDecoder(resp.Body).Decode(&profile); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// Insert headers so that the page is not cached and force the page to be reprocessed from scratch.
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Header().Set("Cache-Control", "private, no-cache, no-store, must-revalidate")
		w.Header().Set("Expires", "Sat, 01 Jan 2000 00:00:00 GMT")
		w.Header().Set("Pragma", "no-cache")
		// Render user edit profile page.
		tmpl := template.Must(template.ParseFiles("assets/html/edit-profile.html"))
		tmpl.Execute(w, profile)
	}

}

// PostEditProfile : Communicate with the backend server.
func PostEditProfile(w http.ResponseWriter, r *http.Request) {

	log.Println("Endpoint: /edit-profile POST")

	fullName := r.FormValue("fullName")
	address := r.FormValue("address")
	telephone := r.FormValue("telephone")
	username := r.FormValue("email")
	profile := Profile{FullName: fullName, Address: address, Telephone: telephone, Username: username}
	cookie, err := r.Cookie("access_token")

	if err != nil {
		log.Println(err)
	}
	reqBody, err := jsoniter.Marshal(profile)
	if err != nil {
		log.Println(err)
	}
	// Prepare the request for the backend server.
	req, err := http.NewRequest("POST", "http://localhost:8000/api/v1/profile", bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.AddCookie(cookie)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
	}
	respCode := resp.StatusCode
	// Verify the response code from backend server.
	if respCode != http.StatusOK {
		// Render the same page again with the corresponding errors.
		errorMessage, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		profileError := Profile{Username: username, Err: true, ErrText: string(errorMessage)}
		tmpl := template.Must(template.ParseFiles("assets/html/edit-profile.html"))
		tmpl.Execute(w, profileError)
	} else {
		http.SetCookie(w, resp.Cookies()[0])
		// Redirect to the profile page.
		http.Redirect(w, r, "http://localhost/profile", http.StatusSeeOther)
	}
}
